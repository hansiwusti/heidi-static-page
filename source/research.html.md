---
title: Trauerzentrum Frankfurt / IBBE e.V.
---

<br>

<!-- Header -->

<header class="bubblehead color0">
<div class="row h-100 w-100 no-gutters align-items-center ">
<div class="container">
<div class="container-fluid">
<div class="col-12 text-center">
<div class="row no-gutters">
  
<!--one-->
<div class="col-lg-4 col-sm-12">

  <div class="rounded-circle r1" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/research.png">
  </div>

  <h2>Forschungsprojekte</h2>

  <p><a href="#forschungsprojekte" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div>

<!--two-->
<div class="col-lg-4 col-sm-12">
  <div class="rounded-circle r2" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/literature.png">
  </div>

  <h2>Literatur</h2>

  <p><a href="#literatur" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div>

<!--four-->
<div class="col-lg-4 col-sm-12">
<div class="rounded-circle r3" id="rectangle">
<img class="col d-flex  icon" src="/images/icons/links.png">
  </div>

  <h2>Links</h2>

  <p><a href="#links" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div><!-- /.col-lg-4 -->
  </div><!-- /.row -->


</div>
</div>
</div>
</div>
</header>

<!-- Content -->



<h3><span id="forschungsprojekte"></span></h3>
<section class="py-5 color1">
  <div class="container">


### Forschungsprojekte

Das Trauerzentrum Frankfurt ist aktuell in folgende Forschungsprojekte eingebunden:

Research Project: „Wo steht die Trauerversorgung in Deutschland? Eine versorgungsepidemiologische Untersuchung“ in Kooperation mit dem Universitätsklinikum Gießen und Marburg, Standort Gießen, der Goethe Universität Frankfurt am Main und der Evangelischen Hochschule Rheinland-Westfalen-Lippe.  

Research Project: „Entwicklung eines Verfahrens zur Früherkennung schwieriger Trauerverläufe bei Angehörigen in der Palliativversorgung“ in Kooperation mit dem Universitätsklinikum Gießen und Marburg, Standort Gießen und der Goethe Universität Frankfurt am Main. 

</div></section>


<h3><span id="literatur"></span></h3>
<section class="py-5 color0">
  <div class="container">

### Literatur 

##### Artikel (Auswahl)
- Müller, H., Münch, U., Bongard, S., Sibelius, U., Berthold, D. (2021). Trauerversorgung in Deutschland. Entwurf eines gestuften Versorgungsmodells, in: Zeitschrift für Evidenz, Fortbildung und Qualität im Gesundheitswesen.  

- Müller, H., Kiepke-Ziemes, S., Albang, M., Münch, U. (2020). Trauer im palliativen Kontext: eine Definition, in: Zeitschrift für Palliativmedizin, 21, 148-150.

- Müller, H., Berthold, D., Bongard, S., Gramm, J., Hauch, H., Sibelius, U. (2020). Komplizierte Trauer erfassen: ein systematischer Review, in: Psychotherapie, Psychosomatik und Medizinischer Psychologie, 70, 1-9. 

- Münch, U., Müller, H., Deffner, T., Schmude, A. v., Kern, M., Kiepke-Ziemes, S., Radbruch, L. (2020). Empfehlungen zur Unterstützung von belasteten, schwerstkranken, sterbenden und trauernden Menschen in der Corona-Pandemie aus palliativmedizinischer Perspektive, in: Der Schmerz, https://doi.org/10.1007/s00482-020-00483-9, OnlineFirst.


- Müller, H., Münch, U. (2020). Trauern in Zeiten von Covid-19: Über den Moment hinaus gedacht, in: Spiritual Care, DOI: https://doi.org/10.1515/spircare-2020-0070, Ahead of Print. 

- Müller, H., Münch, H., Kuhn-Flammensfeld, N., Kiepke-Ziemes, S., Gramm, J. (2019). Eine gemeinsame Sprache finden – „Psychosozial“ und „Spiritualität, in: Zeitschrift für Palliativmedizin, 20, 287-288. 


##### Bücher/ Buchkapitel
- Müller, H., Willmann, H. (2020). Trauerforschung. Basis für praktisches Handeln. Göttingen: Vandenhoeck & Ruprecht.

- Müller, H. (2019). Trauerprozesse verstehen – wie kann das Duale Prozessmodell und dessen Neuentwicklung (DPM-R) dabei helfen? in: Deutscher Kinderhospizverein e.V. (Hrsg.), Gemeinschaft als Erfahrung und Bindeglied, Esslingen: der hospiz verlag Caro & Cie oHG.

- Müller, H., Willmann, H. (2016): Trauer: Forschung und Praxis verbinden. Zusammenhänge verstehen und nutzen, Göttingen: Vandenhoeck & Ruprecht.



##### Literatur für Betroffene
- Albom, M. (2002). Dienstags bei Morrie. Die Lehre eines Lebens, München: Goldmann. 
- Bonanno, G.A. (2012). Die andere Seite der Trauer. Verlustschmerz und Trauma aus eigener Kraft überwinden, Bielefeld, Basel: Aisthesis Verlag. 



</div></section>


<h3><span id="links"></span></h3>
<section class="py-5 color1">
  <div class="container">

### Links
Der Newsletter “Trauerforschung im Fokus” bietet kostenlos und unkompliziert Einblick in aktuelle Themen und Erkenntnisse der internationalen Trauerforschung. [www.trauerforschung.de](www.trauerforschung.de)


Das Internetprojekt [www.gute-trauer.de](www.gute-trauer.de) verfolgt die Ziele:

- Informationen zum Thema Trauer zugänglich zu machen.

- Menschen in die Lage zu versetzen, eigenverantwortlich und natürlich mit Lebenssituationen wie Tod und Trauer umzugehen.

- Bei der wachsenden Fülle an Angeboten der Trauerbegleitung, -beratung und -therapie Transparenz zu schaffen und Trauernde, wenn sie sich Unterstützung wünschen, bei der Suche nach Hilfe zu unterstützen.

- Auf offene Fragen im Bereich Trauer und Trauerbegleitung hinzuweisen.
 

##### Links zu weiterführenden Seiten

Aeterintas e.V. (Verbraucherinitiative Bestattungskultur)
[www.aeternitas.de]()


Association for Death Education and Counseling
[https://www.adec.org/default.aspx](https://www.adec.org/default.aspx)


Bereavement Network Europe (BNE)
[www.bereavement.eu](www.bereavement.eu)


Deutsche Gesellschaft für Palliativmedizin
[https://www.dgpalliativmedizin.de/](https://www.dgpalliativmedizin.de/)


Deutscher Kinderschutzbund
Bezirksverband Frankfurt am Main e.V.
[https://www.kinderschutzbund-frankfurt.de/](https://www.kinderschutzbund-frankfurt.de/)


PalliativTeam Frankfurt
[ https://www.palliativteam-frankfurt.de/](https://www.palliativteam-frankfurt.de/)


</div></section>
