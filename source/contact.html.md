---
title: Kontakt
---

<br>

<section class="py-5 color0">
  <div class="container">


### Information & Anmeldung
Trauerzentrum Frankfurt / IBBE e.V.  
Christel Ortwein  
Heidi Müller  
Alt-Ginnheim 10  
60431 Frankfurt  
Tel. (0 69) 52 19 56 oder (069) 94 50 75 49  
e-mail: [info@trauerzentrum-frankfurt.de](info@trauerzentrum-frankfurt.de) 


</div>
</section>

<section class="py-5 color1">
  <div class="container">


### Anfahrt

<div class="textwidget custom-html-widget"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2556.909829303376!2d8.646359816065068!3d50.14411867943429!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bd0918dbdb42ed%3A0xb4d2d5d3535cb991!2sChristel+Ortwein!5e0!3m2!1sde!2sde!4v1505407760764" width="400" height="300" frameborder="0" style="border:0" allowfullscreen=""></iframe></div>


</div>
</section>


<section class="py-5 color0">
  <div class="container">


### Über uns 
Das Trauerzentrum Frankfurt wurde 1990 von Christel Ortwein gegründet. Es hat den Schwerpunkt Trauerberatung. Wir unterstützen Betroffene dabei, einen Umgang mit ihrem Verlust zu finden, so dass sie diesen in die eigene Biografie integrieren können. Basis unserer Arbeit sind die aktuellen Erkenntnisse der Trauerforschung, denn sie stellen die Grundlage für verantwortbares Handeln dar.   

Seit seiner Gründung ist das Angebot der Einrichtung stetig umfassender geworden. Es bietet Betroffenen praktische und lebensnahe Unterstützung, überkonfessionell und unabhängig. Die Angebote finden zum Teil in Kooperation mit der Katholischen Familienbildung Frankfurt statt. 
Alle Kursleiterinnen begannen ihre Arbeit im Bereich Trauer nach eigener Betroffenheit.


</div>
</section>
  
<section class="py-5 color1">
  <div class="container">



### Leitung des Trauerzentrum Frankfurts

##### Christel Ortwein  
geboren 1952  
Dipl.-Sozialarbeiterin, Kinder-Psychodramatikerin, NLP-Masterin, Psychodramaleiterin.
Arbeitsbereiche:
Einzelgespräche, Gruppengespräche, Kinder-Psychodrama, Kindergruppen, Supervision, Fortbildung
<br>

##### Heidi Müller
geboren 1971  
Dipl.-Politologin, Trauerberaterin, wissenschaftliche Mitarbeiterin im Bereich Trauerforschung.
Arbeitsbereiche:
Einzelgespräche, Gruppengespräche, Forschung, Vorträge, Seminare

</div>
</section>