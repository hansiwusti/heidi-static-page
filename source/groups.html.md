---
title: Trauerzentrum Frankfurt / IBBE e.V.
---

<br>


<!-- Content -->
<header class="bubblehead color0">
<div class="row h-100 w-100 no-gutters align-items-center ">
<div class="container">
<div class="container-fluid">
<div class="col-12 text-center">
<div class="row no-gutters">

<!--one -->  
<div class="col-lg-4 col-sm-12 center-block">
  <div class="rounded-circle r1" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/conversations.png">
  </div>

  <h2>Gespräche</h2>

  <p><a href="#gespraeche" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div>

<!--two -->  
<div class="col-lg-4 col-sm-12">
  <div class="frame rounded-circle r2 " id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/groups.png">
  </div>

  <h2>Fortlaufende Gruppen</h2>

  <p><a href="#fortlaufendegruppen" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div><!-- /.col-lg-4 -->


<div class="col-lg-4 col-sm-12">
  <div class="rounded-circle r3" id="rectangle">

  <img class="col d-flex  icon" src="/images/icons/online_groups.png">

  </div>

  <h2>Online Gruppen</h2>

  <p><a href="#onlinegruppen" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div><!-- /.col-lg-4 -->


</div><!-- /.row  no-gutters-->
</div>
</div>
</div>
</div>
</header>




<h3><span id="gespraeche"></span></h3>
<section class="py-5 color1">
 <div class="container ">

### Gespräche
Wir bieten allen Menschen Gespräche an, die einen Verlust erlitten haben. In den Gesprächen haben alle individuellen Themen Raum. Dabei kann das Erlebte reflektiert und neue Einsichten gewonnen werden. Eine Orientierung bieten hier vor allem die Stärken und Ressourcen der Betroffenen.

##### Erstgespräche

Erstgespräche finden stets vor der Teilnahme an Einzelgesprächen oder einem Gruppenangebot statt. Sie erfahren hier mehr über die Möglichkeiten und Grenzen der Trauerberatung sowie über die Idee und Umsetzung der Gruppenangebote. Weiterhin lernen Sie alleine die Trauerberaterin kennen und können Ihre Themen bzw. Wünsche besprechen. Abschließend entscheiden Sie gemeinsam mit ihr, ob für Sie Einzelgespräche oder Gruppengespräche in Frage kommen. Bei Bedarf wird Ihnen bei der Suche nach einer/m psychologischen PsychotherapeutIn oder alternativen Angeboten geholfen.

##### Einzelgespräche

Einzelgespräche können in der ersten Zeit nach dem Tod der Krisenintervention dienen, aber auch fortlaufend wahrgenommen werden, um Erlebtes zu bewältigen. Betroffene können ihre individuelle Lage darstellen und besprechen. Sie werden in ihrer Individualität angenommen und in ihrem speziellen Trauererleben beraten.
Die Gesprächsangebote sind überkonfessionell und unabhängig sowie in Deutsch und Englisch möglich.

Termine werden nach Absprache vereinbart.
Eine telefonische Krisenintervention ist ebenfalls möglich. Es entstehen die gleichen Kosten wie für ein Einzelgespräch.

Gebühren:
€ 40,– für Erstgespräche à 60 min.
€ 60,– für Einzelgespräche à 60 min.
</div>
</section>


<h3><span id="fortlaufendegruppen"></span></h3>
<section class="py-5 color0">
 <div class="container">

### Fortlaufende Gruppen

Trauer ist eine natürliche Reaktion auf eine Verlusterfahrung und dient der Verarbeitung. Somit ist Trauer ein normaler, gesunder und gewünschter Prozess, der nicht erst erlernt werden muss, auch wenn er sich häufig fremd anfühlt und schmerzhaft ist. Soziale Unterstützung ist wichtig bei der Verlustverarbeitung. Doch durch die gesamtgesellschaftlichen Veränderungen verfügt nicht jeder über ein ausreichend großes soziales Umfeld vor Ort oder möchte Familienangehörige und Freunde mit seinen Erfahrungen belasten.

Wir bieten Gruppen an für Menschen

- nach dem Verlust des Lebenspartners/der Lebenspartnerin
- nach dem Verlust eines Familienangehörigen, z.B. Eltern, Großeltern, Geschwister
- nach dem Verlust eines Kindes während der Schwangerschaft und rund um die Geburt
- nach dem Verlust eines Kindes (Online-Angebot)

Innerhalb von Trauergruppen wird Ihnen im Gespräch mit anderen die Möglichkeit gegeben, über alles zu sprechen, was mit dem Verlust und den Anforderungen des neuen Lebensalltags zusammenhängt. Durch den Austausch mit anderen kann sich die teilweise isolierte Situation verändern.

Trauergruppen bieten:

- die Begegnung mit Gleichbetroffenen
- den Austausch über Verlustreaktionen
- die Normalisierung von Verlustreaktionen
- das Besprechen von Gedanken, Ängsten, Befürchtungen und Wünschen
- die Stärkung der eigenen Fähigkeiten
- die Vermittlung von Hoffnung
- die Entwicklung neuer Lebensperspektiven
- Hilfen zur Selbsthilfe (z.B. zur Reduktion des Grübelns)
- sozialen Anschluss

Eine Anmeldung ist erforderlich.

Gebühren:
€ 150,– für insgesamt 10 Gruppentreffen, jeweils à 90 min.

##### Informationsveranstaltung

Alle Interessierten an einem Gruppenangebot werden vor Beginn zu einer Informationsveranstaltung eingeladen. Den Termin erfahren Sie, wenn Sie beim Trauerzentrum Frankfurt auf der Interessentenliste stehen und die Leiterin der Trauergruppe Sie anruft und dazu einlädt.
Der inhaltliche wie organisatorische Ablauf der 10 Gruppentreffen wird hier erläutert sowie alle Fragen zur Teilnahme an einer solchen Gruppe beantwortet. Gleichzeitig haben Sie Gelegenheit, die anderen GruppenteilnehmerInnen kennenzulernen.
Für die Teilnahme an einer Informationsveranstaltung entstehen keine Kosten.
</div>
</section>



<h3><span id="onlinegruppen"></span></h3>
<section class="py-5 color1">
 <div class="container">

### Online-Gruppenangebot für Eltern, die sich über den Verlust ihres Kindes austauschen möchten

Das eigene Kind zu verlieren, ist ein äußerst harter Schicksalsschlag und bringt betroffene Mütter und Väter häufig in einen absoluten Ausnahmezustand. Eltern sind in diesen schwierigen Momenten oft auf sich allein gestellt, jeder Halt bricht weg. Der emotionale Extremzustand, in dem sie sich befinden, lässt sich kaum vermitteln oder nachvollziehen. Was Eltern in dieser Krise als hilfreich empfinden, kann sehr unterschiedlich sein. Aber fast alle wünschen sich den Austausch mit Gleichbetroffenen. Über die eigenen Erfahrungen und Gefühle zu sprechen, gelingt mit Personen, die ein gleiches Schicksal teilen, viel eher und besser.  

Das Trauerzentrum Frankfurt bietet betroffenen Müttern und Vätern die Möglichkeit, sich online unter fachlicher Anleitung auszutauschen.  

Vor der Teilnahme an der Gruppe ist ein Vorgespräch mit der Trauerberaterin notwendig. Voraussetzung ist auch das Vorhandensein eines Internetfähigen Gerätes mit Bild- und Tonübertragung.  

Eine Anmeldung ist erforderlich. Nach einer verbindlichen Anmeldung werden Informationen über die Internetplattform, deren Nutzung und Zugangsdaten verschickt.  


Gebühren:  € 75,– für insgesamt 5 Gruppentreffen, jeweils à 90 min.

</div>
</section>



<h3><span id="onlinegruppen"></span></h3>
<section class="py-5 color0">
 <div class="container">



### Gruppenangebot bei Verlust des Kindes während der Schwangerschaft oder rund um die Geburt 
Schwangerschaft und Geburt sind bedeutende Lebenserfahrungen. Doch manchmal verlaufen sie anders als erhofft. Bei einigen Frauen endet die Schwangerschaft schon sehr früh, bei anderen verstirbt das Kind in einer späteren Phase, z.B. während der Geburt oder kurz danach.  

Der frühe Verlust des Kindes ist eine Grenzerfahrung, die mit viel Leid und Schmerz einhergehen kann. Die Gefühle und die Trauer mit dem sozialen Umfeld zu teilen, fällt Betroffenen häufig schwer. Viele fühlen sich isoliert und allein gelassen.  

Dieses Gruppenangebot bietet Betroffenen die Möglichkeit, sich unter fachlicher Anleitung mit Gleichbetroffenen auszutauschen, ihren Schmerz sowie ihre Erfahrungen zu teilen und ein besseres Verständnis für ihre Situation durch die Gruppe zu erfahren.  

Eine Anmeldung ist erforderlich.  

Kontakt: Geesche Martin, M.A. Psychosoziale Beratung, Hebamme  
Tel. 069-599761 oder info@krisenberatung-frankfurt.com  

Gebühren: € 112,50,- für insgesamt 5 Gruppentreffen, jeweils à 90 min.  

</div>
</section>