---
title: Trauerzentrum Frankfurt / IBBE e.V.
---




  

<header class="masthead">
<div class="container h-100 w-100">
<div class="row h-100 align-items-center">
<div class="col-12 text-center">

# Trauerzentrum Frankfurt / IBBE e.V.

### Christel Ortwein und Heidi Müller

Trauer lässt sich tragen, wenn sie geteilt werden kann – und neuer Lebensmut kann leichter gefasst werden, wenn Trauernde nicht alleine sind.
</div>
</div>
</div>
</header>

<section class="py-5 color0" style="color:red">
<div class="container">
Anregungen, was Trauernde für sich in Zeiten von COVID-19 tun können oder
wie das soziale Umfeld Trauernde momentan unterstützen kann, finden Sie hier:
[https://www.gute-trauer.de/](https://www.gute-trauer.de/)
</div>
</section>

<section class="py-5 color0">
 <div class="container">

# Aktuelle Termine

### Offener Treff für Witwen und Witwer 2022

Der offene Treff richtet sich an Witwen und Witwer, deren Verlust schon einige Zeit zurückliegt. Er dient dazu, auch nach zeitlich größerem Abstand zur Verlusterfahrung mit anderen Betroffenen in Kontakt zu bleiben und sich sowohl über das Erlebte sowie Freizeitaktivitäten und Urlaube auszutauschen.

### Wann:

Der Offene Treff kann leider bis auf Weiteres nicht stattfinden. Sobald sich an dieser Situation etwas ändert, geben wir Ihnen Bescheid.

### Wo:

Katholische Familienbildung
Tituscorso 2b, Frankfurt-Nordweststadt
Keine Anmeldung notwendig

### Tag des Friedhofs: 18.09.2022

Ort: Frankfurter Hauptfriedhof

</div>
</section>