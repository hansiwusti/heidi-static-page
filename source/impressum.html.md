---
title: Trauerzentrum Frankfurt / IBBE e.V.
---

<br>
<section class="py-5">
  <div class="container">



# Impressum  

Trauerzentrum Frankfurt  
Institut für Beratung und Begleitung von Entwicklungsprozessen IBBE e. V.  
Alt-Ginnheim 10  
D-60431 Frankfurt  
Tel. (0 69) 52 19 56 oder (069) 94 50 75 49  
www.trauerzentrum-frankfurt.de  
info@trauerzentrum-frankfurt.de  


Ein Missbrauch der hier aufgeführten Daten zu anderen Zwecken als dem durch § 6 TDG und § 10 MDStV vorgesehenen Zweck wird hiermit ausdrücklich untersagt. Insbesondere wünschen wir auch keine Werbung an die oben aufgeführten Kontaktmöglichkeiten. Wir weisen darauf hin, dass wir einen Missbrauch der im folgenden aufgeführten Daten straf- und zivilrechtlich verfolgen werden.


Inhaltlich Verantwortliche gem. § 5 TMG: Christel Ortwein, Heidi Müller


Erstellung dieser Seiten: Daniel Ochs   
Landschaftsfotos: TODO copyright for pictures

[Haftungsausschluss](http://www.disclaimer.de/disclaimer.htm)

</div>
</section>