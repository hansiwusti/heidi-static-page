---
title: Trauerzentrum Frankfurt / IBBE e.V.
---

<br>

<!-- Header -->
<header class="bubblehead color0">
<div class="row h-100 w-100 no-gutters align-items-center ">
<div class="container">
<div class="container-fluid">
<div class="col-12 text-center">
<div class="row no-gutters">

<!--one-->
<div class="col-lg-4 col-sm-12">
  <div class="rounded-circle r1" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/pflege.png">


  </div>
  <h2>Pflege</h2>
  <p><a href="#pflege" role="button"> <button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div>

<!--two-->
<div class="col-lg-4 col-sm-12">
  <div class="rounded-circle r2" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/peadagogik.png">

  </div>
  <h2>Pädagogischer Bereich</h2>
  <p><a href="#peadagogik" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div>

<!--three-->
<div class="col-lg-4 col-sm-12">
  <div class="rounded-circle r3" id="rectangle">
  <img class="col d-flex  icon" src="/images/icons/company.png">

  </div>
  <h2>Unternehmen</h2>
  <p><a href="#unternehmen" role="button"><button  type="button" class="btn btn-dark" >Mehr erfahren</button></a></p>
</div><!-- /.col-lg-4 -->

</div><!-- /.row -->

</div>
</div>
</div>
</div>
</header>


<!-- Content -->

<h3><span id="pflege"></span></h3>
<section  class="py-5 color1" >
  <div class="container">



### Hospizlicher und palliativer Bereich, Wohn-, Betreuungs- und Pflegeeinrichtungen

Die Personen, die im hospizlich-/ palliativen Bereich oder auch in Wohn-, Alten- und Pflegeeinrichtungen tätig sind, sind wie kaum eine andere Berufsgruppe mit dem Thema Trauer konfrontiert. Häufig bieten die Einrichtungen niedrigschwellige Angebote für An- und Zugehörige nach dem Verlust an. 

Trauerberatung ist eine verantwortungsvolle Aufgabe und braucht eine gute fachliche Grundlage. Die Trauerforschung bietet kontinuierlich neue Erkenntnisse, um PraktikerInnen in ihrem Handeln zu unterstützen. 

Zudem sind pflegerische und medizinische Fachkräfte wie kaum eine andere Berufsgruppe in ihrem Arbeitsleben mit Sterben, Tod und Trauer konfrontiert. Wichtig ist, dass diese Fachkräfte gut für sich selbst sorgen sowie bei Bedarf Unterstützung in Anspruch nehmen können.      

Wir bieten professionelle Unterstützung in folgenden Bereichen:

- Informationsvermittlung zum Thema Trauer (leicht verständlich, auf aktuellen wissenschaftlichen Erkenntnissen basierend)

- Entwicklung von individuellen Unterstützungssystemen 

- Einzelfallberatung  

<br>

Inhalt, Termin und Kosten nach Absprache. 


</div></section>


<h3><span id="peadagogik"></span></h3>
<section class="py-5 color0" >
  <div class="container">

### Pädagogischer Bereich

Fachkräfte im pädagogischen Bereich sind in hohem Maße verantwortlich für einen sensiblen Umgang mit dem Thema Trauer. Viele Fachkräfte in diesem Bereich verfügen über ein Basiswissen zum Thema Sterben, Tod und Trauer. Hilfreich für diese Berufsgruppe sind häufig ergänzende Informationen aus der aktuellen Trauerforschung.  

Wir bieten professionelle Unterstützung in folgenden Bereichen: 

- Informationsvermittlung zu den Themen Sterben, Tod und Trauer (auf aktuellen wissenschaftlichen Erkenntnissen basierend)

- Kindgerechte Vermittlungsmöglichkeiten

- Unterstützung der Erwachsenen (Eltern, ErzieherInnen)

- Unterstützung der Kinder, Jugendlichen  


<br>

Inhalt, Termin und Kosten nach Absprache.


</div></section>


<h3><span id="unternehmen"></span></h3>
<section class="py-5 color1">
  <div class="container">


### Unternehmen

ArbeitgeberInnen, die sich auch mit sensiblen Themen wie Sterben, Tod und Trauer befassen, tragen maßgeblich zum Erhalt der Arbeitsfähigkeit ihrer Mitarbeiter bei. So empfinden Betroffene zum Beispiel die Rückkehr an den Arbeitsplatz als große Herausforderung. Diese Unsicherheit erleben auch die KollegInnen, die häufig nicht wissen, wie sie mit den Betroffenen umgehen sollen. Um die Arbeitsfähigkeit zu erhalten, ist die Implementierung von Wissen und Umgangsformen mit Verlust und Trauer von Bedeutung. Auf diese Weise können Erwartungen und Unterstützung zum Wohle aller aufeinander abgestimmt werden.      

Wir bieten professionelle Unterstützung in folgenden Bereichen:  

- Informationsvermittlung zu den Themen Sterben, Tod und Trauer (leicht verständlich, auf aktuellen wissenschaftlichen Erkenntnissen basierend)

- Entwicklung von individuellen Unterstützungssystemen 

- Einzelfallberatung  

<br>

Inhalt, Termin und Kosten nach Absprache.





</div>
</section>